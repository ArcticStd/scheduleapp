﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleApp {
    public interface ISQLite {
        string GetDatabasePath (string filename);
    }
}