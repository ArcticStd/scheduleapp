﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ScheduleApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AllStudentsPage : ContentPage
	{
        Users user;
        bool deleteMode;
        public AllStudentsPage(Users user)
        {
            InitializeComponent();
            this.user = user;
            if(user.isAdmin == 0)
            {
                Createbtn.IsVisible = false;
                Deletebtn.IsVisible = false;
            }
            User.Text = user.SecondName +" "+(user.Name)[0] + "." + (user.ThirdName)[0] + ".";
            ChangeFilter.ItemsSource = new List<string> { "ФИО", "Группа" };
            ShowStudents();

        }
        private async void EditStudent(object sender, SelectedItemChangedEventArgs e)
        {
            
            if (e.SelectedItem != null)
            {
                if (!deleteMode)
                {
                    StudentPage page = new StudentPage(StudentsList,user.isAdmin);
                    page.BindingContext = (Student)e.SelectedItem;
                    page.InitializePickers();
                    await Navigation.PushModalAsync(page);
                }
                else
                {
                    if (await DisplayAlert("Подтверждение", "Вы действительно хотите удалить этого студента?", "Удалить", "Отмена"))
                    {
                        await App.Studentbase.DeleteItemAsync((Student)e.SelectedItem);
                        StudentsList.ItemsSource = await App.Studentbase.GetItemsAsync();
                    }
                    else
                    {
                        StudentsList.SelectedItem = null;
                    }
                    await Deletebtn.ScaleTo(1,250,Easing.CubicOut);
                }
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        private async void SearchPerson(object sender, TextChangedEventArgs e)
        {
            StudentsList.ItemsSource = await App.Studentbase.FilterByName(e.NewTextValue);
        }
        async void ShowStudents()
        {
            StudentsList.ItemsSource = await App.Studentbase.GetItemsAsync();
        }
        async void CreateStudent(object sender, EventArgs e)
        {
            StudentPage page = new StudentPage(StudentsList,1);
            page.BindingContext = new Student();
            page.InitializePickers();
            await Task.WhenAll(Createbtn.ScaleTo(1.3,250,Easing.CubicIn), Navigation.PushModalAsync(page));
            await Createbtn.ScaleTo(1, 250, Easing.CubicOut);
        }      
        async void DeleteStudent(object sender,EventArgs e)
        {
            if (!deleteMode)
            {
                deleteMode = true;
                await Deletebtn.ScaleTo(1.5, 250, Easing.CubicIn);
            }
            else
            {
                deleteMode = false;
            }
        }
        async void FilterList(object sender, EventArgs e)
        {
            switch (((Picker)sender).SelectedIndex)
            {
                case 0:
                    StudentsList.ItemsSource = await App.Studentbase.FilterBy(0);
                    break;
                case 1:
                    StudentsList.ItemsSource = await App.Studentbase.FilterBy(1);
                    break;
            }

        }
        
	}
}