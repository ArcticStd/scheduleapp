﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ScheduleApp {
    [XamlCompilation (XamlCompilationOptions.Compile)]
    public partial class Forget_password : ContentPage {
        bool Ischecked = false;
        bool IsEnd = false;
        Users current_user = null;
        public Forget_password () {
            InitializeComponent ();
        }

        private async void NextStep (object sender, EventArgs e) {

            if (!IsEnd)
                switch (Ischecked) {
                    case true:

                        if (EntryCell.Text.ToUpper () == current_user.Answer.ToUpper ()) {
                            EmailorPhone.Text = "Ваш пароль был успешно сброшен.\nВведите новый пароль.";
                            EntryCell.Placeholder = "Новый пароль";
                            EntryCell.Text = "";
                            EntryCell.IsPassword = true;
                            PasswordCell.IsVisible = true;
                            PasswordCell.IsPassword = true;
                            EmailorPhone.TextColor = Color.FromHex ("#3B4043");;
                            IsEnd = true;
                        } else {
                            EmailorPhone.Text = "Вы ввели неправильный ответ.";
                            EmailorPhone.TextColor = Color.FromHex ("#944949");
                        }
                        break;
                    case false:
                        if (!(await App.Userbase.CheckEmail (EntryCell.Text, EntryCell.Text))) {
                            current_user = await App.Userbase.TakeUser ();
                            Ischecked = true;
                            EmailorPhone.Text = $"Введите ответ на Ваш секретный вопрос\n{current_user.secretAnswer}";
                            EntryCell.Placeholder = "Ответ на секретный вопрос";
                            EntryCell.Text = "";
                            EmailorPhone.TextColor = Color.FromHex ("#3B4043");;
                        } else {
                            EmailorPhone.Text = "Введенный вами email или телефон не существует.";
                            EmailorPhone.TextColor = Color.FromHex ("#944949");
                        }
                        break;
                }
            else {
                if (PasswordCell.Text != EntryCell.Text) {
                    EmailorPhone.Text = "Введенные Вами пароли не совпадают.";
                    EmailorPhone.TextColor = Color.FromHex ("#944949");
                } else {
                    current_user.Password = PasswordCell.Text;
                    await App.Userbase.SaveItemAsync (current_user);
                    await Navigation.PopModalAsync ();
                }
            }
        }
    }
}