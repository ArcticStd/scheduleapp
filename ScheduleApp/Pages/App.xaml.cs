﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace ScheduleApp {
    public partial class App : Application {

        //Таблицы для данных студентов
        public const string USERBASE_NAME = "users.db";
        public const string SEX = "sex.db";
        public const string EDUCATIONSTATUS = "EducationStatus.db";
        public const string ZACHISLENIE = "Zachislenie.db";
        public const string MILLITARYSTATUS = "MillitariStatus.db";
        public const string HOSTELSTATUS = "HostelStatus.db";
        public const string GRANTSTATUS = "GrantStatus.db";
        public static StudentRepositoty studentbase;
        public static SexRepository sexbase;
        public static EducationStatusRepository educationbase;
        public static ZachislenieRepository zachisleniebase;
        public static MillitariStatusRepository millitaribase;
        public static HostelStatusRepository hostelbase;
        public static GrantStatusRepository grantbase;
        //Таблицы для данных пользователей
        public const string STUDENTBASE_NAME = "students.db";
        public const string SECRETANSWER = "secret_answer.db";
        public static SecretAnswerRepositoty secretbase;
        public static UsersRepository userbase;
        public static SecretAnswerRepositoty Secretbase {
            get {
                if (secretbase == null) {
                    secretbase = new SecretAnswerRepositoty (SECRETANSWER);

                }
                return secretbase;
            }
        }
        public static SexRepository Sexbase {
            get {
                if (sexbase == null) {
                    sexbase = new SexRepository (SEX);

                }
                return sexbase;
            }
        }
        public static EducationStatusRepository Educationbase {
            get {
                if (educationbase == null) {
                    educationbase = new EducationStatusRepository (EDUCATIONSTATUS);

                }
                return educationbase;
            }
        }
        public static ZachislenieRepository Zachisleniebase {
            get {
                if (zachisleniebase == null) {
                    zachisleniebase = new ZachislenieRepository (ZACHISLENIE);

                }
                return zachisleniebase;
            }
        }
        public static MillitariStatusRepository Millitaribase {
            get {
                if (millitaribase == null) {
                    millitaribase = new MillitariStatusRepository (MILLITARYSTATUS);

                }
                return millitaribase;
            }
        }
        public static HostelStatusRepository Hostelbase {
            get {
                if (hostelbase == null) {
                    hostelbase = new HostelStatusRepository (HOSTELSTATUS);

                }
                return hostelbase;
            }
        }
        public static GrantStatusRepository Grantbase {
            get {
                if (grantbase == null) {
                    grantbase = new GrantStatusRepository (GRANTSTATUS);

                }
                return grantbase;
            }
        }
        public static UsersRepository Userbase {
            get {
                if (userbase == null) {
                    userbase = new UsersRepository (USERBASE_NAME);
                }
                return userbase;
            }
        }
        public static StudentRepositoty Studentbase {
            get {
                if (studentbase == null) {
                    studentbase = new StudentRepositoty (STUDENTBASE_NAME);

                }
                return studentbase;
            }
        }
        public App () {
            InitializeComponent ();
            MainPage = new NavigationPage (new LogInPage ());
        }
        protected override void OnStart () {
            // Handle when your app starts
        }

        protected override void OnSleep () {
            // Handle when your app sleeps
        }

        protected override void OnResume () {
            // Handle when your app resumes
        }
    }
}