﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ScheduleApp {
    [XamlCompilation (XamlCompilationOptions.Compile)]
    public partial class LogInPage : ContentPage {
        bool isSign = false;
        bool isrestore = false;
        public LogInPage () {
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar (this, false);
        }

        void RegistreMe (object sender, EventArgs e) {
            try {
                Navigation.PushModalAsync (new RegistrationPage ());
            } catch (Exception ex) {
                DisplayAlert ("Error", ex.Message, "Back");
            }
            //Navigation.PushAsync(new RegistrationPage(),false);
        }

        async void SignIn (object sender, EventArgs e) {
            if (!isSign) {

                isSign = true;
                if (await App.Userbase.IsRegistre (Emailorphone.Text, Password.Text)) {
                    var user = await App.Userbase.TakeUser ();
                    var AllStudentsPage = new AllStudentsPage (user);
                    var t = await App.Studentbase.GetItemsAsync ();
                    var p = await App.Userbase.GetItemsAsync ();
                    AllStudentsPage.BindingContext = App.Studentbase.GetItemsAsync ();
                    await Navigation.PushModalAsync (AllStudentsPage);
                } else
                    isSign = false;
            }
        }

        async void RemovePassword (object sender, EventArgs e) {
            if (!isrestore) {
                isrestore = true;
                await Navigation.PushModalAsync (new Forget_password ());
            }

        }
        protected override void OnAppearing () {
            isSign = false;
            isrestore = false;
            base.OnAppearing ();
        }
    }
}