﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ScheduleApp {
    [XamlCompilation (XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage {
        public RegistrationPage () {
            InitializeComponent ();
            PlaceAnswers ();
        }

        async void PlaceAnswers () {
            try {
                SecretAnswer.ItemsSource = (await App.Secretbase.GetItemsAsync ()).Select (x => x.Name).ToList<String> ();
            } catch (Exception ex) {
                await DisplayAlert ("Error", ex.Message, "Back");
            }

        }
        async void RegistreMe (object sender, EventArgs e) {
            if (Name.Text != null &&
                SecondName.Text != null &&
                Email.Text != null &&
                Password.Text != null &&
                Answer.Text != null &&
                SecretAnswer.SelectedIndex != -1) {
                if (await App.Userbase.CheckEmail (Email.Text, Phone.Text)) {
                    await App.Userbase.SaveItemAsync (new Users {
                        Name = Name.Text,
                            SecondName = SecondName.Text,
                            ThirdName = ThirdName.Text,
                            Email = Email.Text,
                            Phone = Phone.Text,
                            Password = Password.Text,
                            SecretAnswer = SecretAnswer.SelectedIndex,
                            Answer = Answer.Text
                    });
                    Alert.IsVisible = false;
                    await Registration.ScaleTo (0, 100, Easing.CubicOut);
                    Registration.IsVisible = false;
                    Succesfull.IsVisible = true;
                    await Succesfull.ScaleTo (1, 100, Easing.CubicIn);
                } else {
                    Alert.IsVisible = true;
                }
            }
        }
        void Back (object sender, EventArgs e) {
            Navigation.PopModalAsync ();
        }
    }
}