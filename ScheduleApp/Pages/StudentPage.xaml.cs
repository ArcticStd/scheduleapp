﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ScheduleApp {
    [XamlCompilation (XamlCompilationOptions.Compile)]
    public partial class StudentPage : ContentPage {
        //public static readonly BindableProperty Selecteditemproperty =
        //    BindableProperty.Create("SelectItem", typeof(string), typeof(Picker), default(string));
        ListView list;
        public StudentPage (ListView list, int isAdmin) {
            this.list = list;
            InitializeComponent ();
            if (isAdmin == 0) {
                DisableElements ();
            }
            NavigationPage.SetHasNavigationBar (this, false);

        }
        void DisableElements () {
            Name.IsEnabled = false;
            SecondName.IsEnabled = false;
            ThirdName.IsEnabled = false;
            Sex.IsEnabled = false;
            Phone.IsEnabled = false;
            EducationStatus.IsEnabled = false;
            Zachislenie.IsEnabled = false;
            MillitariStatus.IsEnabled = false;
            HostelStatus.IsEnabled = false;
            MillitariStatus.IsEnabled = false;
            Group.IsEnabled = false;
            Grant.IsEnabled = false;
            SaveBtn.IsVisible = false;
        }
        public void InitializePickers () {
            SexPicker ();
            EducationPicker ();
            ZachisleniePicker ();
            MillitariPicker ();
            HostelPicker ();
            GrantPicker ();
        }
        void SelectSex (object sender, EventArgs e) {
            ((Student) BindingContext).Sex = ((Picker) sender).SelectedIndex;
        }
        private async void SexPicker () {
            Sex.ItemsSource = (await App.Sexbase.GetItemsAsync ()).Select (x => x.Name).ToList<String> ();
            Sex.SelectedIndex = Convert.ToInt32 (((Student) BindingContext).Sex);
        }
        void SelectEducation (object sender, EventArgs e) {
            ((Student) BindingContext).EducationStatus = ((Picker) sender).SelectedIndex;
        }
        private async void EducationPicker () {
            EducationStatus.ItemsSource = (await App.Educationbase.GetItemsAsync ()).Select (x => x.Name).ToList<String> ();
            EducationStatus.SelectedIndex = Convert.ToInt32 (((Student) BindingContext).EducationStatus);
        }
        void SelectZachislenie (object sender, EventArgs e) {
            ((Student) BindingContext).Zachislenie = ((Picker) sender).SelectedIndex;
        }
        private async void ZachisleniePicker () {
            Zachislenie.ItemsSource = (await App.Zachisleniebase.GetItemsAsync ()).Select (x => x.Name).ToList<String> ();
            Zachislenie.SelectedIndex = Convert.ToInt32 (((Student) BindingContext).Zachislenie);
        }
        void SelectMillitari (object sender, EventArgs e) {
            ((Student) BindingContext).MillitariStatus = ((Picker) sender).SelectedIndex;
        }
        private async void MillitariPicker () {
            MillitariStatus.ItemsSource = (await App.Millitaribase.GetItemsAsync ()).Select (x => x.Name).ToList<String> ();
            MillitariStatus.SelectedIndex = Convert.ToInt32 (((Student) BindingContext).MillitariStatus);
        }
        void SelectHostel (object sender, EventArgs e) {
            ((Student) BindingContext).HostelStatus = ((Picker) sender).SelectedIndex;
        }
        private async void HostelPicker () {
            HostelStatus.ItemsSource = (await App.Hostelbase.GetItemsAsync ()).Select (x => x.Name).ToList<String> ();
            HostelStatus.SelectedIndex = Convert.ToInt32 (((Student) BindingContext).HostelStatus);
        }
        void SelectGrant (object sender, EventArgs e) {
            ((Student) BindingContext).GrantStatus = ((Picker) sender).SelectedIndex;
        }
        private async void GrantPicker () {
            Grant.ItemsSource = (await App.Grantbase.GetItemsAsync ()).Select (x => x.Name).ToList<String> ();
            Grant.SelectedIndex = Convert.ToInt32 (((Student) BindingContext).GrantStatus);
        }
        private async void SaveStudent (object sender, EventArgs e) {
            var current_student = (Student) BindingContext;
            string name = current_student.Name;
            string secondname = current_student.SecondName;
            string thirdname = current_student.ThirdName;
            int sex = current_student.Sex;
            string phone = current_student.Phone;
            int education = current_student.EducationStatus;
            int zachislenie = current_student.Zachislenie;
            int millitarystatus = current_student.MillitariStatus;
            string group = current_student.Group;
            int grant = current_student.GrantStatus;
            if (!String.IsNullOrEmpty (name) &&
                !String.IsNullOrEmpty (secondname) &&
                !String.IsNullOrEmpty (thirdname) &&
                !String.IsNullOrEmpty (phone) &&
                !String.IsNullOrEmpty (group) &&
                education != -1 &&
                sex != -1 &&
                zachislenie != -1 &&
                millitarystatus != -1 &&
                grant != -1
            ) {
                Succesfullornot.IsVisible = true;
                Succesfullornot.Text = "Сохранено";
                Succesfullornot.TextColor = Color.Green;
                await App.Studentbase.SaveItemAsync (current_student);
                list.ItemsSource = await App.Studentbase.GetItemsAsync ();

            } else {
                Succesfullornot.IsVisible = true;
                Succesfullornot.Text = "Вы ввели некоретные данные!";
                Succesfullornot.TextColor = Color.FromHex ("#944949");
            }
            await Task.Delay (2000);
            Succesfullornot.Text = "";
            Succesfullornot.IsVisible = false;
        }
        private async void DeleteStudent (object sender, EventArgs e) {
            var Student = (Student) BindingContext;

            await Navigation.PopAsync ();
        }
        private async void Cancel (object sender, EventArgs e) {
            list.SelectedItem = null;
            await Navigation.PopModalAsync ();

        }
        protected override bool OnBackButtonPressed () {
            list.SelectedItem = null;
            return base.OnBackButtonPressed ();
        }
    }
}