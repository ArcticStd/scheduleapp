﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;

namespace ScheduleApp {

    public class UsersRepository {
        SQLiteAsyncConnection database;
        string current_user = null;
        public UsersRepository (string filename) {
            string databasePath = DependencyService.Get<ISQLite> ().GetDatabasePath (filename);
            database = new SQLiteAsyncConnection (databasePath);

        }
        public async Task CreateTable () {
            await database.CreateTableAsync<Users> ();
        }
        public async Task<List<Users>> GetItemsAsync () {
            return await database.Table<Users> ().ToListAsync ();

        }

        public async Task<Users> TakeUser () {
            return (await database.FindAsync<Users> (x => x.Email.ToUpper () == current_user.ToUpper () || x.Phone == current_user.ToUpper ()));
        }
        public async Task<bool> CheckEmail (string EmailPhone, string Phone) {
            var t = await database.FindAsync<Users> (x => x.Email.ToUpper () == EmailPhone.ToUpper () || x.Phone == Phone.ToUpper ());
            if (t == null) {
                current_user = null;
                return true;
            } else {
                current_user = EmailPhone;
                return false;
            }
        }
        public async Task<bool> IsRegistre (string EmailorPhone, string password) {
            Users t = await database.FindAsync<Users> (x => x.Email.ToUpper () == EmailorPhone.ToUpper () || x.Phone == EmailorPhone.ToUpper ());
            if (t != null && t.Password == password) {

                current_user = EmailorPhone;
                return true;
            } else {

                current_user = null;
                return false;
            }
        }
        public async Task<Users> GetItemAsync (int id) {
            return await database.GetAsync<Users> (id);
        }
        public async Task<int> DeleteItemAsync (Users item) {
            return await database.DeleteAsync (item);
        }
        public async Task<int> SaveItemAsync (Users item) {
            if (item.Id != 0) {
                await database.UpdateAsync (item);
                return item.Id;
            } else {
                return await database.InsertAsync (item);
            }
        }
    }

    public class SecretAnswerRepositoty {
        SQLiteAsyncConnection database;
        private SQLiteConnection testdate;

        public SecretAnswer GetSecretAm (int id) {
            return testdate.Get<SecretAnswer> (id);
        }
        public SecretAnswerRepositoty (string filename) {
            string databasePath = DependencyService.Get<ISQLite> ().GetDatabasePath (filename);
            database = new SQLiteAsyncConnection (databasePath);
            testdate = new SQLiteConnection (databasePath);
        }
        public async Task<SecretAnswer> GetItemAsync (int id) {
            var t = (await database.GetAsync<SecretAnswer> (id));
            return t;
        }
        public async Task CreateTable () {
            await database.CreateTableAsync<SecretAnswer> ();
        }
        public async Task<List<SecretAnswer>> GetItemsAsync () {
            return await database.Table<SecretAnswer> ().ToListAsync ();

        }
    }
}