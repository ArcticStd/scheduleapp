﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;

namespace ScheduleApp {
    public class StudentRepositoty {
        SQLiteAsyncConnection database;
        public StudentRepositoty (string filename) {
            string databasePath = DependencyService.Get<ISQLite> ().GetDatabasePath (filename);
            database = new SQLiteAsyncConnection (databasePath);

        }

        public async Task<List<Student>> FilterBy (int variant) {
            switch (variant) {
                case 1:
                    return await database.Table<Student> ().OrderBy (x => x.Group).ToListAsync ();

                default:
                    return await database.Table<Student> ().OrderBy (x => x.SecondName).ThenBy (x => x.Name).ThenBy (x => x.ThirdName).ToListAsync ();
            }
        }
        public async Task CreateTable () {
            await database.CreateTableAsync<Student> ();
        }
        public async Task<List<Student>> GetItemsAsync () {
            return await database.Table<Student> ().ToListAsync ();

        }
        public async Task<List<Student>> FilterByName (string name) {
            //return database.Table<Student>().Where(x => ($"{x.SecondName}{x.Name}{x.ThirdName}").Contains(name)).ToListAsync();
            var datab = await database.Table<Student> ().ToListAsync ();
            return (from student in datab
                let fullname = $"{student.SecondName} {student.Name} {student.ThirdName}"
                where ((String) fullname).Contains (name) select student).ToList ();

        }
        public async Task<Student> GetItemAsync (int id) {
            return await database.GetAsync<Student> (id);
        }
        public async Task<int> DeleteItemAsync (Student item) {
            return await database.DeleteAsync (item);
        }
        public async Task<int> SaveItemAsync (Student item) {
            if (item.Id != 0) {
                await database.UpdateAsync (item);
                return item.Id;
            } else {
                return await database.InsertAsync (item);
            }
        }
    }

    public class SexRepository {
        SQLiteAsyncConnection database;
        public SexRepository (string filename) {
            string databasePath = DependencyService.Get<ISQLite> ().GetDatabasePath (filename);
            database = new SQLiteAsyncConnection (databasePath);
        }
        public async Task CreateTable () {
            await database.CreateTableAsync<Sex> ();
        }
        public async Task<List<Sex>> GetItemsAsync () {
            return await database.Table<Sex> ().ToListAsync ();

        }
    }
    public class EducationStatusRepository {
        SQLiteAsyncConnection database;
        public EducationStatusRepository (string filename) {
            string databasePath = DependencyService.Get<ISQLite> ().GetDatabasePath (filename);
            database = new SQLiteAsyncConnection (databasePath);

        }
        public async Task CreateTable () {
            await database.CreateTableAsync<EducationStatus> ();
        }
        public async Task<List<EducationStatus>> GetItemsAsync () {
            return await database.Table<EducationStatus> ().ToListAsync ();

        }
    }
    public class ZachislenieRepository {
        SQLiteAsyncConnection database;
        public ZachislenieRepository (string filename) {
            string databasePath = DependencyService.Get<ISQLite> ().GetDatabasePath (filename);
            database = new SQLiteAsyncConnection (databasePath);

        }
        public async Task CreateTable () {
            await database.CreateTableAsync<Zachislenie> ();
        }
        public async Task<List<Zachislenie>> GetItemsAsync () {
            return await database.Table<Zachislenie> ().ToListAsync ();

        }
    }
    public class MillitariStatusRepository {
        SQLiteAsyncConnection database;
        public MillitariStatusRepository (string filename) {
            string databasePath = DependencyService.Get<ISQLite> ().GetDatabasePath (filename);
            database = new SQLiteAsyncConnection (databasePath);

        }
        public async Task CreateTable () {
            await database.CreateTableAsync<MillitariStatus> ();
        }
        public async Task<List<MillitariStatus>> GetItemsAsync () {
            return await database.Table<MillitariStatus> ().ToListAsync ();

        }
    }
    public class HostelStatusRepository {
        SQLiteAsyncConnection database;
        public HostelStatusRepository (string filename) {
            string databasePath = DependencyService.Get<ISQLite> ().GetDatabasePath (filename);
            database = new SQLiteAsyncConnection (databasePath);

        }
        public async Task CreateTable () {
            await database.CreateTableAsync<HostelStatus> ();
        }
        public async Task<List<HostelStatus>> GetItemsAsync () {
            return await database.Table<HostelStatus> ().ToListAsync ();

        }
    }
    public class GrantStatusRepository {
        SQLiteAsyncConnection database;
        public GrantStatusRepository (string filename) {
            string databasePath = DependencyService.Get<ISQLite> ().GetDatabasePath (filename);
            database = new SQLiteAsyncConnection (databasePath);

        }
        public async Task CreateTable () {
            await database.CreateTableAsync<GrantStatus> ();
        }
        public async Task<List<GrantStatus>> GetItemsAsync () {
            return await database.Table<GrantStatus> ().ToListAsync ();

        }
    }
}