﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLiteNetExtensions;
using SQLiteNetExtensions.Attributes;

namespace ScheduleApp {
    [Table ("Users")]
    public class Users {
        [PrimaryKey, AutoIncrement, Column ("_id")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string ThirdName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }

        [Ignore]
        public string secretAnswer {
            get {
                return App.Secretbase.GetSecretAm (SecretAnswer + 1).Name;
            }
        }

        public int SecretAnswer { get; set; }
        public string Answer { get; set; }
        public int isAdmin { get; set; } = 0;
    }

    [Table ("SecretAnswer")]
    public class SecretAnswer {
        [PrimaryKey, AutoIncrement, Column ("_id")]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}