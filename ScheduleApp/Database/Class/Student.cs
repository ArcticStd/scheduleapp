﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace ScheduleApp {
    [Table ("Students")]
    public class Student {
        [PrimaryKey, AutoIncrement, Column ("_id")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string ThirdName { get; set; }
        public int Sex { get; set; } = -1;
        public string Phone { get; set; }
        public int EducationStatus { get; set; } = -1;
        public int Zachislenie { get; set; } = -1;
        public int MillitariStatus { get; set; } = -1;
        public int HostelStatus { get; set; } = -1;
        public string Group { get; set; }
        public int GrantStatus { get; set; } = -1;
    }

    [Table ("Sex")]
    public class Sex {
        [PrimaryKey, AutoIncrement, Column ("_id")]
        public int ID { get; set; }
        public string Name { get; set; }
    }

    [Table ("EducationStatus")]
    public class EducationStatus {
        [PrimaryKey, AutoIncrement, Column ("_id")]
        public int ID { get; set; }
        public string Name { get; set; }
    }

    [Table ("Zachislenie")]
    public class Zachislenie {
        [PrimaryKey, AutoIncrement, Column ("_id")]
        public int ID { get; set; }
        public string Name { get; set; }
    }

    [Table ("MillitariStatus")]
    public class MillitariStatus {
        [PrimaryKey, AutoIncrement, Column ("_id")]
        public int ID { get; set; }
        public string Name { get; set; }
    }

    [Table ("HostelStatus")]
    public class HostelStatus {
        [PrimaryKey, AutoIncrement, Column ("_id")]
        public int ID { get; set; }
        public string Name { get; set; }
    }

    [Table ("GrantStatus")]
    public class GrantStatus {
        [PrimaryKey, AutoIncrement, Column ("_id")]
        public int ID { get; set; }
        public string Name { get; set; }
    }
}