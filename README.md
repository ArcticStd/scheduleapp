# About Project
***
### It's not a commertional project. You may use this code the way you want. The Project was created using Xamairin in Visual Studio. 
### It was a course project made in 2017, and it's aimed to simplify the work with large number of students.

# About Code
***
* ### .xaml pages are located in:
ScheduleApp / ScheduleApp / Pages /
* ### Database classes are located in:
ScheduleApp / ScheduleApp / Database /
* ### App connects with database using class located in:
ScheduleApp / ScheduleApp.$PLATFORM$ / SQLite_$PLATFORM$.cs